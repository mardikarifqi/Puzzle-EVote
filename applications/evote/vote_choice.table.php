<?php
$a = new DatabaseTableBuilder();

$a->addColumn("nim","VARCHAR(20)");
$a->addColumn("kertassuara","INT");
$a->addColumn("choice","INT");
$a->addColumn("token","VARCHAR(6)");

$a->createIndex("id",["nim","kertassuara"],"UNIQUE");
$a->createIndex("token",["token"]);
$a->createIndex("kertassuara",["kertassuara"]);

return $a;
?>