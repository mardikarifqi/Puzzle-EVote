<?php
$a = new DatabaseTableBuilder();

$a->addColumn("nim","VARCHAR(20)");
$a->addColumn("nama");
$a->addColumn("angkatan","CHAR(4)");

$a->createIndex("NIM",["NIM"],"UNIQUE");
return $a;
?>