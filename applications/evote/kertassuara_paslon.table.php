<?php
$a = new DatabaseTableBuilder();

$a->addColumn("id","INT")->setAsPrimaryKey(true)->defaultValue("AUTO_INCREMENT");
$a->addColumn("kertassuara","INT");
$a->addColumn("paslon","INT");

$a->createIndex("id",["kertassuara","paslon"], "UNIQUE");

return $a;
?>