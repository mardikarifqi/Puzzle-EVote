<?php
$a = new DatabaseTableBuilder();

$a->addColumn("token","VARCHAR(6)")->setAsPrimaryKey();
$a->addColumn("nim","VARCHAR(20)");
$a->addColumn("created","INT");

return $a;
?>