<?php
use EVote\Auth;

spl_autoload_register(function($c){
	if(starts_with($c,"EVote\\")){
		$file = my_dir("class/".strtolower(str_replace("EVote\\","",$c)).".php");
		if(file_exists($file)) include($file);
	}
});

$appProp->bundle["pub"] = IO::publish(my_dir("assets"));

if(Accounts::authAccess(USER_AUTH_SU)){
	include "app/su.php";
}elseif(Accounts::authAccessAdvanced(Auth::panitia_pusat())){
	include "app/panpus.php";
}elseif(Accounts::authAccessAdvanced(Auth::bilik_pemilih())){
	include "app/bilik.php";
}elseif(Accounts::authAccessAdvanced(Auth::penjaga_tps())){
	include "app/tps.php";
}else{
	$appProp->http_code = 404;
}
?>