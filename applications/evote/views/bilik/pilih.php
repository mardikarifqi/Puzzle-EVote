<?php
use EVote\KertasSuara;
?>

<!-- CSS -->
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<!-- JavaScript -->
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

<style>
.dcalon{
    cursor:pointer;
    transition:filter .1s;
}
.dcalon:hover{
    filter:brightness(1)!important;
}
.dcalon:active{
    background:red;
    color:white;
    filter:brightness(1)!important;
}
.kertassuara_d:hover .dcalon{
    filter:brightness(.5);
}
</style>

<div class="page">
    <?php foreach($kertassuara as $kertas):?>
    <div class="carousel" style="width:100%" kid="<?php echo $kertas?>">
        <?php
            $kertas = new KertasSuara($kertas);
            $paslon = $kertas->kandidat;
            $title = $kertas->judul;
            $golput = $kertas->bisa_golput;
            include my_dir("views/desain/kertassuara.php");
        ?>
    </div>
    <?php endforeach?>
</div>

<?php ob_start()?>
<script>
(function(){
    var token = "<?php echo $token?>";
    var kc = "<?php echo count($kertassuara)?>";
    var frame = $('.page').flickity({
        cellAlign: 'left',
        contain: true,
        prevNextButtons: false,
        draggable:false,
        pageDots: false
    });
    $(".dcalon").on("click",function(e){
        if(!confirm("Kunci Pilihan?")) return;
        var car = $(this).closest(".carousel");
        var kertassuara = car.attr("kid");
        var choice = this.getAttribute("paslonid");
        kc--;
        frame.flickity('next');
        car.css("opacity",0);
        if(kc == 0) {
            setTimeout(() => {
                location.reload();
            }, 500);
        }
        $.post(location.href,{
            submit:1,
            act:"pilih",
            token:token,
            kertassuara:kertassuara,
            pilihan:choice
        });
    });
}());
</script>
<?php echo Minifier::outJSMin()?>