<style>
body{
    padding:0;
}
#identitas td{
    padding:5px 15px;
}
.nama_pemilih{
    text-transform:uppercase;
}
</style>

<div style="display:flex;justify-content:center;align-items:center;width:100vw;height:100vh">
    <div style="max-width: 600px;
        margin-top: -5vh;
        padding: 25px;
        border-radius: 10px;
        box-shadow: 0 0 5px -1px black;">
        <label>
            <p>1. Masukkan NIM</p>
            <div class="input-group">
                <form method="post" id="user_search">
                    <input autocomplete="off" autofocus type="text" name="nim" class="form-control" style="margin-left:15px;width:400px;">   
                    <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Cari</button>
                    </div>
                </form>
            </div>
        </label>
        <div class="showmore" id="datapemilih" style="display:none">
            <br><br>
            <p><b>2. Identitas Mahasiswa</b></p>
            <table id="identitas" style="width:500px">
                <tr>
                    <td width="1"><b>Nama</b></td>
                    <td width="1">:</td>
                    <td class="nama_pemilih"></td>
                </tr>
                <tr>
                    <td width="1"><b>Angkatan</b></td>
                    <td width="1">:</td>
                    <td class="angkatan"></td>
                </tr>
                <tr>
                    <td style="padding-top:20px;color:red;" colspan="3"><b>PASTIKAN NAMA DI KARTU IDENTITAS SESUAI</b></td>
                </tr>
            </table><br><br>
            <p><b>3. Generate Token</b></p>
            <p style="margin-left:15px"><button class="btn btn-default gtok">Generate Token!</button></p>
        </div>
        <div class="showmore" style="display:none;font-size: 56pt;text-align: center;font-weight: bold;" id="tokenform">234dvb</div>
    </div>
</div>

<?php ob_start()?>
<script>
(function(){
    var searching = false;
    var data = null;

    $(".gtok").click(function(){
        if(data == null) return;
        $.post(location.href,{
            submit:1,
            act:"generate_token",
            pemilih:data.nim,
            json:1
        },function(d){
            d = JSON.parse(d);
            if(d.token !== false && d.success){
                $(".showmore").hide();
                $("#tokenform").show().html(d.token);
            }else{
                showMessage("Pemilih tidak diperkenankan membuat token lagi","danger");
            }
        });
    });

    $("#user_search").submit(function(e){
        e.preventDefault();
        if(searching) return;
        searching = true;
        data = null;
        $(".showmore").hide();
        var fd = new FormData(this);
        fd.append("submit",1);
        fd.append("act","cari_pemilih");
        fd.append("json",1);
        $.ajax({
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            type: "post",
            url: location.href,
            success: function(d){
                searching = false;
                if(d.success){
                    var y=$("#datapemilih").slideDown();
                    y.find(".nama_pemilih").html(d.pemilih.nama);
                    y.find(".angkatan").html(d.pemilih.angkatan);
                    data = d.pemilih;
                }else{
                    data = null;
                    showMessage("Pemilih tidak terdaftar","danger");
                }
            }
        });
    });
}());
</script>
<?php echo Minifier::outJSMin()?>