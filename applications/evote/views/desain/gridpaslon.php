<?php 
use EVote\Paslon;
?>

<style>
.paslon_c{
    text-align:center;
    padding: 15px;
    border: 2px solid black;
    text-transform:uppercase;
    margin-bottom:25px;
    cursor:pointer;
}
.paslon_c .nama{
    font-size: 14pt;
    font-weight: bold;
    margin-bottom:15px
}
.paslon_c .foto{
    width:100%;
    object-fit:cover;
    object-position:center;
    height:150px;
    margin-bottom:15px
}
.paslon_c .aa div{
    padding:0;
}
</style>

<div class="row">
<?php foreach(Paslon::getAll() as $p):?>
<div class="col-md-3">
    <div class="paslon_c" pid="<?php echo $p["id"]?>">
        <div class="row aa">
            <div class="col-xs-<?php echo $p["tipe"]==1 ? 6 : 12?>">
                <img src="<?php echo UserData::getURL($p["foto1"])?>" class="foto">
            </div>
            <?php if($p["tipe"] == 1):?>
            <div class="col-xs-6">
                <img src="<?php echo UserData::getURL($p["foto2"])?>" class="foto">
            </div>
            <?php endif?>
        </div>
        <div><?php echo $p["kandidat1"]?></div>
        <div><?php echo $p["tipe"] == 1 ? $p["kandidat2"] : "<br>"?></div>
    </div>
</div>
<?php endforeach?>
</div>