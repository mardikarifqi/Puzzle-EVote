<?php 
use EVote\Paslon;
if(!defined("_CSS_KK")): define("_CSS_KK",1); ob_start();?>
<style>
@import url('https://fonts.googleapis.com/css?family=Volkhov:700');

.kertassuara_d{
    width:1200px;
    height:min-content;
    padding:20px;
    transform: scale(.9);
    transform-origin: top center;
    margin: auto;
    background:#fff;
}
.kertassuara_d .header{
    display:flex;
    height:70px;
}
.kertassuara_d .ctn{
    padding:20px;
    border: solid 4px black;
    margin:20px 0;
    text-align:center;
}
.kertassuara_d .title{
    text-align:center;
}
.kertassuara_d .header img{
    height:100%;
    margin-right:5px;
}
.kertassuara_d .dcalon{
    width: 30%;
    flex: 0 0 248px;
    margin: 15px;
    margin-top:0;
    position:relative;
}
.kertassuara_d .vote_rating{
    background: #F44336;
    position: absolute;
    left: 0;
    right: 0;
    top: 45px;
    z-index: 99;
    font-size: 14px;
    padding: 1px;
    color: white;
    width: 100%;
}
.kertassuara_d .num{
    font-size: 14pt;
    border: 2px solid black;
    border-radius: 50%;
    width: 35px;
    margin: auto;
    height: 35px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 10px;
}
.kertassuara_d .foto{
    height: 146px;
    margin:0
}
.kertassuara_d .foto > div{
    padding:0;
}
.kertassuara_d .foto img{
    width:100%;
    margin-right:5px;
    object-fit:cover;
    object-position:center;
}
.kertassuara_d .l{
    border: 2px solid black;
}
.kertassuara_d .pname{
    text-transform:uppercase;
    padding: 10px;
    background: white;
    border: 2px solid black;
    margin-top: -2px;
}
</style>
<?php echo Minifier::outCSSMin(); endif;?>

<div class="kertassuara_d">
    <div class="header">
        <img src="<?php echo $pub?>/img/unair.png" alt="">
        <img src="<?php echo $pub?>/img/logo.jpg" alt="">
        <img src="<?php echo $pub?>/img/pilra.jpg" alt="">
    </div>
    <div class="ctn">
        <div style="font-family:Volkhov, Arial;font-weight:700;font-size:20pt;">
            SURAT SUARA<br>
            PEMILU RAYA 2018<br>
        </div>
        <div class="tname" style="margin-top:5px;font-size:16pt;"><?php echo $title?></div><br>
        <div style="flex-wrap: wrap;display:flex;justify-content: center;">
            <?php 
            foreach($paslon as $k=>$p): 
            $p = Database::readAll("app_evote_paslon","where id='?'", $p)->data[0];
            ?>
            <div class="dcalon" paslonid="<?php echo $p["id"]?>">
                <?php if(isset($percentage)): if(!isset($percentage[$p["id"]])) $percentage[$p["id"]] = 0;?>
                <div class="vote_rating" style="width:<?php echo $percentage[$p["id"]]?>%">
                    <div class="value"><?php echo round($percentage[$p["id"]])?>%</div>
                </div>
                <?php endif?>
                <div class="num"><?php echo $k+1?></div>
                <div class="l">
                    <div class="foto row">
                        <div class="col-xs-<?php echo $p["tipe"]==1 ? 6 : 12?>">
                            <img src="<?php echo UserData::getURL($p["foto1"])?>" class="foto">
                        </div>
                        <?php if($p["tipe"] == 1):?>
                        <div class="col-xs-6">
                            <img src="<?php echo UserData::getURL($p["foto2"])?>" class="foto">
                        </div>
                        <?php endif?>
                    </div>
                </div>
                <div class="pname">
                    <div><?php echo $p["kandidat1"]?></div>
                    <div><?php echo $p["tipe"] == 1 ? $p["kandidat2"] : "<br>"?></div>
                </div>
            </div>
            <?php endforeach?>
            <?php if($golput):?>
            <div class="dcalon">
                <div class="num"><?php echo $k+2?></div>
                <div class="l" style="
                    height: 213px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    font-size: 24pt;">
                    GOLPUT
                </div>
            </div>
            <?php endif?>
        </div>
    </div>
</div>