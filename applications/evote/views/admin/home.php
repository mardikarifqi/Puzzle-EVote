<?php
use EVote\Auth;

$a = new Application("users");
?>

<div style="display:flex;align-items:center;justify-content:center;width:100%;height:100%;">
    <div style="width:400px;margin-top:-40px">
        <form action="" method="POST">
        <h2>Role Pengaturan User Group</h2><hr>
        <div class="row">
            <div class="col-md-4">
                <div>Penjaga TPS</div>
                <?php $a->loadView("group_button",["penjaga_tps",Auth::penjaga_tps(),USER_AUTH_EMPLOYEE]);?><br><br>
            </div>
            <div class="col-md-4">
                <div>Komputer Bilik</div>
                <?php $a->loadView("group_button",["bilik_pemilih",Auth::bilik_pemilih(),USER_AUTH_EMPLOYEE]);?><br><br>
            </div>
            <div class="col-md-4">
                <div>Panitia Pusat</div>
                <?php $a->loadView("group_button",["panitia_pusat",Auth::panitia_pusat(),USER_AUTH_EMPLOYEE]);?><br><br>
            </div>
        </div>
        <br>
        <input type="hidden" name="submit" value="1">
        <button class="btn-success btn">Simpan</button>
        </form>
    </div>
</div>