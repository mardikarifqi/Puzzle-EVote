<?php
use EVote\Paslon;
new Application("upload_img_ajax");
?>

<style>
.pas_prev{
    width: 100%;
    height: 200px;
}
.pas_form{
    text-align:center;
}
</style>

<div class="row">
    <div class="col-md-2">
        <div style="height:120px;width:100%;">
            <img style="width: 100%;height: 100%;object-fit: contain;object-position: center;" src="<?php echo $pub ?>/img/logo.jpg">
        </div><br>
        <?php include "menus.php"?>
    </div>
    <div class="col-md-10">
        <h2>Daftar Paslon</h2><br>
        <div style="display:flex;">
            <button class="add_paslon btn btn-default"><i class="fa fa-plus"></i> Tambah Paslon</button>
            <button class="add_calon btn btn-primary"><i class="fa fa-plus"></i> Tambah Calon</button>
        </div><br>
        <?php include my_dir("views/desain/gridpaslon.php")?>
    </div>
</div>

<div class="modal fade" id="tambah_paslon" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Paslon</h4>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 pas_form gjlkdc">
                            <div id="pas1" class="pas_prev">
                                <?php ImageUploader::dumpPreviewTemplate()?>
                            </div>
                            <div>
                                <?php ImageUploader::dumpForm("foto1","Unggah Foto Paslon 1","default btn-sm",".modal.in #pas1")?>
                            </div><br>
                            <div>
                                <input type="text" class="form-control" name="kandidat1" placeholder="Nama Kandidat 1">
                            </div>
                        </div>
                        <div class="col-md-6 pas_form tpddc">
                            <div id="pas2" class="pas_prev">
                                <?php ImageUploader::dumpPreviewTemplate()?>
                            </div>
                            <div>
                                <?php ImageUploader::dumpForm("foto2","Unggah Foto Paslon 2","default btn-sm",".modal.in #pas2")?>
                            </div><br>
                            <div>
                                <input type="text" class="form-control" name="kandidat2" placeholder="Nama Kandidat 2">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="submit" value="1">
                    <input type="hidden" name="act" value="tambah_paslon">
                    <button type="button" class="btn btn-danger rmb" style="display:none">Hapus</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ob_start()?>
<script>
(function(){
    $("ul .paslon").addClass("active");
    var smnp = false;
    $(".paslon_c").click(function(){
        var id = this.getAttribute("pid");
        $.post(location.href,{
            submit:1,
            act: "load_data",
            id: id
        },function(d){
            d=JSON.parse(d);
            var m = $("#tambah_paslon").clone().appendTo("body");
            m.find("#pas1 div").css("background-image","url('"+d.data.foto1+"')");
            m.find("input[name=kandidat1]").val(d.data.kandidat1);
            m.find(".rmb").show().click(function(){
                var t = $(this).prop("disabled",true);
                $.post(location.href,{
                    submit:1,
                    act: "hapus_pcaslon",
                    json:1,
                    id:id
                },function(d){
                    d=JSON.parse(d);
                    if(d.success){
                        location.reload();
                    }else{
                        t.prop("disabled",false);
                        showMessage(d.reason,"danger");
                    }
                })
            });
            if(d.data.tipe == 1){
                m.find("input[name=kandidat2]").val(d.data.kandidat2);
                m.find("#pas2 div").css("background-image","url('"+d.data.foto2+"')");
                m.find(".gjlkdc").removeClass("col-md-12").addClass("col-md-6");
                m.find('.tpddc').show();                
            }else{
                m.find('.tpddc').hide();
                m.find(".gjlkdc").addClass("col-md-12").removeClass("col-md-6");
            }
            m.find("button[type=submit]")[0].onclick=function(){
                var t = $(this);
                if(m.find("input[name=nama_paslon]").val()=="" && smnp){
                    showMessage("Mohon mengisi semua data!","danger");
                    return;
                }
                if(m.find("input[name=kandidat1]").val()==""){
                    showMessage("Mohon mengisi semua data!","danger");
                    return;
                }
                if(m.find("input[name=kandidat2]").val()=="" && smnp){
                    showMessage("Mohon mengisi semua data!","danger");
                    return;
                }
                
                t.prop('disabled',true);
                $.post(location.href,{
                    submit:1,
                    act: d.data.tipe == 1 ? "edit_paslon" : "edit_calon",
                    kandidat1:m.find("input[name=kandidat1]").val(),
                    kandidat2:m.find("input[name=kandidat2]").val(),
                    json:1,
                    id:id
                },function(d){
                    d=JSON.parse(d);
                    if(d.success){
                        location.reload();
                    }else{
                        t.prop("disabled",false);
                        showMessage("Terjadi Kesalahan","danger");
                    }
                });
            };
            m.modal('show');
            m.on("hidden.bs.modal",function(){
                m.remove()
            });
        });
    });
    $(".add_paslon").click(function(){
        smnp = true;
        $(".gjlkdc").removeClass("col-md-12").addClass("col-md-6");
        $('.tpddc').show();
        $("#tambah_paslon").modal('show');
    });
    $(".add_calon").click(function(){
        smnp = false;
        $('.tpddc').hide();
        $(".gjlkdc").addClass("col-md-12").removeClass("col-md-6");
        $("#tambah_paslon").modal('show');
    });
    $("button[type=submit]")[0].onclick=function(){
        var t = $(this);
        if($("input[name=nama_paslon]").val()=="" && smnp){
            showMessage("Mohon mengisi semua data!","danger");
            return;
        }
        if($("input[name=kandidat1]").val()==""){
            showMessage("Mohon mengisi semua data!","danger");
            return;
        }
        if($("input[name=kandidat2]").val()=="" && smnp){
            showMessage("Mohon mengisi semua data!","danger");
            return;
        }
        
        t.prop('disabled',true);
        $.post(location.href,{
            submit:1,
            act: smnp ? "tambah_paslon" : "tambah_calon",
            kandidat1:$("input[name=kandidat1]").val(),
            kandidat2:$("input[name=kandidat2]").val(),
            json:1
        },function(d){
            d=JSON.parse(d);
            if(d.success){
                location.reload();
            }else{
                t.prop("disabled",false);
                showMessage("Terjadi Kesalahan","danger");
            }
        })
    };
}());
</script>
<?php echo Minifier::outJSMin()?>