<?php
use EVote\Paslon;
use EVote\KertasSuara;
use EVote\Vote;

new Application("upload_img_ajax");
?>

<style>
.pas_prev{
    width: 100%;
    height: 200px;
}
.pas_form{
    text-align:center;
}
</style>

<div class="row">
    <div class="col-md-2">
        <div style="height:120px;width:100%;">
            <img style="width: 100%;height: 100%;object-fit: contain;object-position: center;" src="<?php echo $pub ?>/img/logo.jpg">
        </div><br>
        <?php include my_dir("views/panpus/menus.php"); ?>
    </div>
    <div class="col-md-10">
        <h2>Kertas Suara</h2>
        <div style="position:fixed;bottom:20px;right:20px;z-index:1">
            <button class="add_kk btn btn-default" style="margin-left:10px;"><i class="fa fa-plus"></i> Buat Baru</button>
        </div>
        <div style="margin-bottom:150px;">
            <?php
            foreach (array_reverse(KertasSuara::getAll()) as $kertas) {
                $paslon = $kertas->kandidat;
                $title = $kertas->judul;
                if (UserData::read("sudah_hitung_" . $kertas->id) == 1) {
                    $percentage = Vote::ambil_hasil_grouped($kertas);
                }else{
                    unset($percentage);
                }
                echo "<div style='margin-bottom:20px'>";
                include my_dir("views/desain/kertassuara.php");
                ?>
                    <div style="margin-top: -60px;text-align: center;" kid="<?php echo $kertas->id ?>">
                        <a href="/evote/hitung?id=<?php echo $kertas->id ?>"><button class="ch_kk btn btn-default" style=""><i class="fa fa-eye"></i> Mulai Perhitungan Suara</button></a>
                    </div>
                    <?php
                    echo "</div>";
                }
                ?>
        </div>
    </div>
</div>

<?php ob_start() ?>
<script>
(function(){
    $("ul .hitung").addClass('active');
}());
</script>
<?php echo Minifier::outJSMin() ?>