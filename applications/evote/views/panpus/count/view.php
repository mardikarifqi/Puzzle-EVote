<?php 
use EVote\KertasSuara;
?>

<style>
#paku{
    transition: all .5s ease;
    opacity:0;
    transform:translate(20px,-20px);
}
#paku.coblos{
    transform:unset;
    opacity:1;
}
</style>

<div id="counter" style="
    font-size: 45pt;
    background: white;
    position:fixed;">1
</div>

<div style="width:100%;margin-top:15px;position:relative">
    <?php
        $kertas = $kertassuara;
        $paslon = $kertas->kandidat;
        $title = $kertas->judul;
        $golput = $kertas->boleh_golput;
        include my_dir("views/desain/kertassuara.php");
    ?>
    <div id="paku" style="height: 177px;
        width: 150px;
        position: absolute;
        display:none;">
        <img style="width: 100%;
        height: 100%;
        object-fit: contain;
        object-position: center;" src="<?php echo $pub?>/img/coblos.png" alt="">
    </div>
</div>

<div style="text-align:center;">
    <button class="btn btn-default nextbtn">Berikutnya</button>
</div>

<?php ob_start()?>
<script>
(function(){
    var hasil = JSON.parse('<?php echo str_replace("'","\\'",json_encode($hasil)) ?>');
    var counter = 0;
    var paku = $("#paku");
    var ch = $("#counter");
    var showResult = function(){
        if(counter >= hasil.length){
            showMessage("Perhitungan Suara telah selesai","success");
            setTimeout(() => {
                location = "/evote/hitung";
            }, 1000);
        }
        var h = hasil[counter], o;
        counter++;
        if(h!=-1){
            o = $(".dcalon[paslonid='"+h+"']")[0];
        }else{
            o = $(".dcalon").last()[0];
        }
        paku.css("left",(o.offsetLeft + 240) + "px").css("top",(o.offsetTop - 80) + "px").show().removeClass("coblos");
        setTimeout(() => {
            paku.addClass("coblos");
        }, 500);
        ch.html(counter);
    };
    showResult();
    $(".nextbtn").click(function(){showResult()});
}());
</script>
<?php echo Minifier::outJSMin()?>