<?php
use EVote\Paslon;
use EVote\KertasSuara;
new Application("upload_img_ajax");
?>

<style>
.pas_prev{
    width: 100%;
    height: 200px;
}
.pas_form{
    text-align:center;
}
</style>

<div class="row">
    <div class="col-md-2">
        <div style="height:120px;width:100%;">
            <img style="width: 100%;height: 100%;object-fit: contain;object-position: center;" src="<?php echo $pub ?>/img/logo.jpg">
        </div><br>
        <?php include "menus.php"?>
    </div>
    <div class="col-md-10">
        <h2>Kertas Suara</h2>
        <div style="position:fixed;bottom:20px;right:20px;z-index:1">
            <button class="add_kk btn btn-default" style="margin-left:10px;"><i class="fa fa-plus"></i> Buat Baru</button>
        </div>
        <div style="margin-bottom:150px;">
            <?php
            foreach(array_reverse(KertasSuara::getAll()) as $kertas){
                $paslon = $kertas->kandidat;
                $title = $kertas->judul;
                echo "<div style='margin-bottom:20px'>";
                    include my_dir("views/desain/kertassuara.php");
                    ?>
                    <div style="margin-top: -60px;text-align: center;" kid="<?php echo $kertas->id?>">
                        <div><?php echo implode(", ",$kertas->target)?><br><br></div>
                        <button class="ch_kk btn btn-default" style=""><i class="fa fa-pencil"></i> Ubah</button>
                        <button class="rm_kk btn btn-danger" style="margin-left:10px;"><i class="fa fa-trash"></i> Hapus</button>
                    </div>
                    <?php
                echo "</div>";
            }
            ?>
        </div>
    </div>
</div>

<!-- Paslon Chooser -->
<div class="modal fade" id="pilih_paslon" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Paslon</h4>
            </div>
            <div class="modal-body">
                <?php include my_dir("views/desain/gridpaslon.php")?>
            </div>
        </div>
    </div>
</div>

<!-- Kertas Suara Form -->
<div class="modal fade" id="kertassuara_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubah Kertas Suara</h4>
            </div>
            <div class="modal-body">
                <div><label>Nama Kertas Suara</label></div>
                <input type="text" name="name" class="form-control" placeholder="-" autofocus><br>
                <div><label>Daftar Paslon</label></div>
                <div class="row">
                    <div class="col-md-4 ahere">
                        <div class="ap" style="border:1px solid black;padding:15px;cursor:pointer;">
                            Tambah Paslon
                        </div>
                    </div>
                </div><br>
                <div><label>Target Formulir</label></div>
                <label><input type="checkbox" name="target" value="2015" style="margin-right:5px">2015</label><br>
                <label><input type="checkbox" name="target" value="2016" style="margin-right:5px">2016</label><br>
                <label><input type="checkbox" name="target" value="2017" style="margin-right:5px">2017</label><br>
                <label><input type="checkbox" name="target" value="2018" style="margin-right:5px">2018</label><br>
                <br>
                <label><input type="checkbox" name="bisa_golput" value="yes" style="margin-right:5px">Bisa Golput</label><br>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="submit" value="1">
                <input type="hidden" name="act" value="tambah_paslon">
                <button type="button" class="btn btn-danger rmb" style="display:none">Hapus</button>
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </div>
</div>

<?php ob_start()?>
<script>
(function(){
    $(".ch_kk").click(function(){
        $.post(location.href,{
            submit:1,
            act:"load_kertassuara",
            id:$(this).parent().attr("kid"),
            json:1
        },function(d){
            d = JSON.parse(d);
            if(d.success){
                var choosen = d.data.kandidat;
                var t = $("#kertassuara_form").clone().appendTo("body");
                t.modal('show').on("hidden.bs.modal",function(){t.remove()});
                choosen.forEach(function(c){
                    $("#pilih_paslon .paslon_c[pid="+c+"]").clone().addClass("col-md-4").click(function(){
                        choosen.splice(choosen.indexOf(c),1);
                        $(this).remove();
                    }).insertBefore(t.find(".ahere"));
                });
                t.find("input[name=name]").val(d.data.judul);
                t.find("input[name=bisa_golput]").prop("checked",d.data.bisa_golput);
                d.data.target.forEach(function(i){
                    t.find("input[name=target][value="+i+"]").prop("checked",true);
                });
                t.find(".ap").click(function(){
                    var x = $("#pilih_paslon").clone().appendTo("body");
                    choosen.forEach(function(i){x.find(".paslon_c[pid="+i+"]").parent().remove()});
                    x.modal('show').on("hidden.bs.modal",function(){
                        x.remove();
                        $("body").addClass("modal-open");
                    });
                    x.find(".paslon_c").click(function(){
                        var paslon = parseInt(this.getAttribute("pid"));
                        if(choosen.indexOf(paslon) == -1){
                            $(this).clone().addClass("col-md-4").click(function(){
                                choosen.splice(choosen.indexOf(paslon),1);
                                $(this).remove();
                            }).insertBefore(t.find(".ahere"));
                            choosen.push(paslon);
                        }else{
                            showMessage("Paslon sudah ada di dalam kertas suara","danger");
                        }
                        x.modal("hide");
                    });
                });
                //Posting
                t.find("button[type=submit]").click(function(){
                    var name = t.find("input[name=name]").val();
                    var bisa_golput = t.find("input[name=bisa_golput]:checked").length > 0 ? 1 : 0;
                    var paslon = choosen;
                    var target = [];
                    t.find("input[name=target]:checked").each(function(){
                        target.push(this.value);
                    });
                    if(name == ""){
                        showMessage("Nama tidak boleh kosong","danger");
                        return;
                    }
                    if(paslon.length < 2){
                        showMessage("Pilih minimal 2 paslon","danger");
                        return;
                    }
                    if(target.length < 1){
                        showMessage("Pilih minimal 1 target angkatan","danger");
                        return;
                    }
                    var btn = $(this).prop("disabled",true);
                    $.post(location.href,{
                        submit:1,
                        act:"ubah_kertassuara",
                        nama:name,
                        paslon:JSON.stringify(paslon),
                        target:JSON.stringify(target),
                        id:parseInt(d.data.id),
                        bisa_golput:bisa_golput,
                        json:1
                    },function(d){
                        d = JSON.parse(d);
                        if(d.success){
                            location.reload();
                        }else{
                            showMessage("Terjadi kesalahan","danger");
                            btn.prop("disabled",false);
                        }
                    });
                });
            }else{
                showMessage("Terjadi kesalahan","danger");
            }
        });
    });
    $(".rm_kk").click(function(){
        $.post(location.href,{
            submit:1,
            act:"hapus_kertassuara",
            id:$(this).parent().attr("kid"),
            json:1
        },function(d){
            d = JSON.parse(d);
            if(d.success){
                location.reload();
            }else{
                showMessage("Terjadi kesalahan","danger");
            }
        });
    });
    $("ul .kertassuara").addClass('active');
    $(".add_kk").click(function(){
        var choosen = [];
        var t = $("#kertassuara_form").clone().appendTo("body");
        t.modal('show').on("hidden.bs.modal",function(){t.remove()});
        t.find(".ap").click(function(){
            var x = $("#pilih_paslon").clone().appendTo("body");
            choosen.forEach(function(i){
                x.find(".paslon_c[pid="+i+"]").parent().remove();
            });
            x.modal('show').on("hidden.bs.modal",function(){
                x.remove();
                $("body").addClass("modal-open");
            });
            x.find(".paslon_c").click(function(){
                var paslon = parseInt(this.getAttribute("pid"));
                if(choosen.indexOf(paslon) == -1){
                    $(this).clone().addClass("col-md-4").click(function(){
                        choosen.splice(choosen.indexOf(paslon),1);
                        $(this).remove();
                    }).insertBefore(t.find(".ahere"));
                    choosen.push(paslon);
                }else{
                    showMessage("Paslon sudah ada di dalam kertas suara","danger");
                }
                x.modal("hide");
            });
        });
        //Posting
        t.find("button[type=submit]").click(function(){
            var name = t.find("input[name=name]").val();
            var bisa_golput = t.find("input[name=bisa_golput]:checked").length > 0 ? 1 : 0;
            var paslon = choosen;
            var target = [];
            t.find("input[name=target]:checked").each(function(){
                target.push(this.value);
            });
            if(name == ""){
                showMessage("Nama tidak boleh kosong","danger");
                return;
            }
            if(paslon.length < 2){
                showMessage("Pilih minimal 2 paslon","danger");
                return;
            }
            if(target.length < 1){
                showMessage("Pilih minimal 1 target angkatan","danger");
                return;
            }
            var btn = $(this).prop("disabled",true);
            $.post(location.href,{
                submit:1,
                act:"tambah_kertassuara",
                nama:name,
                paslon:JSON.stringify(paslon),
                target:JSON.stringify(target),
                bisa_golput:bisa_golput,
                json:1
            },function(d){
                d = JSON.parse(d);
                if(d.success){
                    location.reload();
                }else{
                    showMessage("Terjadi kesalahan","danger");
                    btn.prop("disabled",false);
                }
            });
        });
    });
}());
</script>
<?php echo Minifier::outJSMin()?>