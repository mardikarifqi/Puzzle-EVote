<link rel="stylesheet" href="<?php echo $pub ?>/css/tabulator.min.css">
<link href="<?php echo $pub ?>/css/semantic-ui/tabulator_semantic-ui.min.css" rel="stylesheet">
<script src="<?php echo $pub ?>/js/tabulator.min.js"></script>

<div class="row">
    <div class="col-md-2">
        <div style="height:120px;width:100%;">
            <img style="width: 100%;height: 100%;object-fit: contain;object-position: center;" src="<?php echo $pub ?>/img/logo.jpg">
        </div><br>
        <?php include "menus.php"?>
    </div>
    <div class="col-md-10">
        <h2>Daftar Pemilih</h2><br>
        <div style="display:flex;">
            <button data-toggle="modal" data-target="#tambah_pemilih" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
            <a href="/evote/csvpemilih"><button class="btn"><i class="fa fa-file-excel-o"></i> Download CSV</button></a>
            <button data-toggle="modal" data-target="#upload_csv" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Import CSV</button>
        </div><br>
        <div id="data-pemilih"></div>
    </div>
</div>

<div class="modal fade" id="tambah_pemilih" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Pemilih</h4>
            </div>
            <form method="POST">
                <div class="modal-body">
                    <div>
                        <label style="width:100%">
                            <span>NIM</span>
                            <input autocomplete="off" type="text" name="nim" class="form-control" required>
                        </label>
                    </div><br>
                    <div>
                        <label style="width:100%">
                            <span>Nama Mahasiswa</span>
                            <input autocomplete="off" type="text" name="nama" class="form-control" required>
                        </label>
                    </div><br>
                    <div>
                        <label style="width:100%"><span>Tahun Angkatan</span></label>
                        <label><input type="radio" name="tahun_angkatan" value="2015" checked> 2015</label><br>
                        <label><input type="radio" name="tahun_angkatan" value="2016"> 2016</label><br>
                        <label><input type="radio" name="tahun_angkatan" value="2017"> 2017</label><br>
                        <label><input type="radio" name="tahun_angkatan" value="2018"> 2018</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="submit" value="1">
                    <input type="hidden" name="act" value="tambah_pemilih">
                    <button type="submit" class="btn btn-success" >Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="upload_csv" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Impor Data Pemilih</h4>
            </div>
            <form method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="file" accept=".csv" name="csv" required>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="submit" value="1">
                    <input type="hidden" name="act" value="upload_csv">
                    <button type="submit" class="btn btn-success" >Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php ob_start()?>
<script>
$("ul .pemilih").addClass("active");
var table = new Tabulator("#data-pemilih", {
    layout:"fitColumns", 
    pagination:"local", //enable local pagination.
    paginationSize:25,
    columns:[
        {title:"NIM <i class='fa fa-key'></i>", field: "nim", sorter:"integer", headerFilter:"input"},
        {title:"Nama Mahasiswa",sorter:"string", field: "nama", editor:"input", headerFilter:"input" ,mutatorEdit:function(value, data, type, params, component){
            $.post(location.href,{submit:1,act:"edit_nama_pemilih",edit:1,data:JSON.stringify(data),nama:value},function(r){
                try{
                    r = JSON.parse(r);
                    if(r.success) showMessage("Data dirubah","success");
                    else showMessage("Terjadi Kesalahan","danger");
                }catch(e){
                    showMessage("Terjadi Kesalahan","danger");
                }
            });
            return value;
        }},
        {title:"Angkatan", field: "angkatan", sorter:"integer",  headerFilter:"input", editor:"select", editorParams:{
            "2015":"2015",
            "2016":"2016",
            "2017":"2017",
            "2018":"2018",
        },mutatorEdit:function(value, data, type, params, component){
            $.post(location.href,{submit:1,act:"edit_angkatan_pemilih",edit:1,data:JSON.stringify(data),angkatan:value},function(r){
                try{
                    r = JSON.parse(r);
                    if(r.success) showMessage("Data dirubah","success");
                    else showMessage("Terjadi Kesalahan","danger");
                }catch(e){
                    showMessage("Terjadi Kesalahan","danger");
                }
            });
            return value;
        }},
        {title:"Hak Pilih", field: "franchise", sorter:"integer",formatter:function(cell, formatterParams){
			return cell.getValue() + " kertas suara.";
		}},
        {title:"",formatter:function(cell, formatterParams){
            return '<button class="btn btn-danger btn-sm" type="button">Hapus</button>';
        },cellClick:function(e, cell){
            if(!confirm("Hapus Pemilih?")) return;
            $.post(location.href,{submit:1,act:"hapus_pemilih",nim:cell.getData().nim},function(r){
                try{
                    r = JSON.parse(r);
                    if(r.success){
                        showMessage("Data dihapus","success");
                        cell.getRow().delete();
                    }else showMessage("Terjadi Kesalahan","danger");
                }catch(e){
                    showMessage("Terjadi Kesalahan","danger");
                }
            });
        }}
    ]
});
table.setData("/evote/data/pemilih");
table.redraw();
</script>
<?php echo Minifier::outJSMin()?>