<?php
namespace EVote;

define("DATA_CALON", 2);

class Calon extends Paslon
{
    public function save(){
        if (!isset($this->kandidat1)) throw new \PuzzleError("Mohon isi data dengan lengkap!");

        if (!$this->loaded) {
            if (!file_exists($this->temp_foto1)) throw new \PuzzleError("Mohon isi data dengan lengkap!");
        }

        $this->tipe = DATA_CALON;
        $this->write();
    }
}
?>