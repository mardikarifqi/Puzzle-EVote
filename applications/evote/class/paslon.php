<?php
namespace EVote;

define("DATA_PASLON", 1);

class Paslon implements \JsonSerializable
{
    protected $id;
    protected $kandidat1;
    protected $kandidat2;
    protected $foto1;
    protected $foto2;

    protected $temp_foto1;
    protected $temp_foto2;

    protected $tipe = DATA_PASLON;

    protected $loaded = false;

    public function __construct($id = null)
    {
        if ($id !== null) {
            $data = \Database::readAll("app_evote_paslon", "where id='?'", $id)->data[0];
            if (isset($data["id"])) {
                $this->id = $data["id"];
                $this->kandidat1 = $data["kandidat1"];
                $this->kandidat2 = $data["kandidat2"];
                $this->foto1 = $data["foto1"];
                $this->foto2 = $data["foto2"];
                $this->tipe = $data["tipe"];
                $this->loaded = true;
            } else {
                throw new \PuzzleError("Data tidak ditemukan");
            }
        }
    }

    protected function write()
    {
        $a = new \DatabaseRowInput;
        $a->setField("kandidat1", $this->kandidat1);
        $a->setField("kandidat2", $this->kandidat2);
        $a->setField("tipe", $this->tipe);

        if (file_exists($this->temp_foto1)) {
            $this->foto1 = "foto1_" . rand_str(15);
            \UserData::move($this->foto1, $this->temp_foto1);
            $a->setField("foto1", $this->foto1);
        }

        if (file_exists($this->temp_foto2)) {
            $this->foto2 = "foto2_" . rand_str(15);
            \UserData::move($this->foto2, $this->temp_foto2);
            $a->setField("foto2", $this->foto2);
        }

        if (!$this->loaded) {
            if (\Database::newRowAdvanced("app_evote_paslon", $a)) {
                $this->loaded = 1;
                $this->id = \Database::max("app_evote_paslon", "id");
                return true;
            } else {
                return false;
            }
        } else {
            return \Database::updateRowAdvanced("app_evote_paslon", $a, "id", $this->id);
        }
    }

    public function save()
    {
        if (!isset($this->kandidat1)) throw new \PuzzleError("Mohon isi data dengan lengkap!");
        if (!isset($this->kandidat2)) throw new \PuzzleError("Mohon isi data dengan lengkap!");

        if (!$this->loaded) {
            if (!file_exists($this->temp_foto1)) throw new \PuzzleError("Mohon isi data dengan lengkap!");
            if (!file_exists($this->temp_foto2)) throw new \PuzzleError("Mohon isi data dengan lengkap!");
        }

        return $this->write();
    }

    public function __set($var, $val)
    {
        switch ($var) {
            case "kandidat1":
                $this->kandidat1 = htmlentities($val);
                break;
            case "kandidat2":
                $this->kandidat2 = htmlentities($val);
                break;
            case "foto1":
                if (!file_exists($val)) throw new \PuzzleError("File tidak ditemukan");
                $this->temp_foto1 = $val;
                break;
            case "foto2":
                if (!file_exists($val)) throw new \PuzzleError("File tidak ditemukan");
                $this->temp_foto2 = $val;
                break;
        }
    }

    public function __get($var)
    {
        switch ($var) {
            case "kandidat1":
                return $this->kandidat1;
            case "kandidat2":
                return $this->kandidat2;
            case "id":
                return $this->id;
            case "foto1":
                return \UserData::getURL($this->foto1);
            case "foto2":
                return \UserData::getURL($this->foto2);
        }
    }

    public function jsonSerialize()
    {
        return [
            "kandidat1" => $this->kandidat1,
            "kandidat2" => $this->kandidat2,
            "id" => $this->id,
            "foto1" => \UserData::getURL($this->foto1),
            "foto2" => \UserData::getURL($this->foto2),
            "tipe" => $this->tipe
        ];
    }

    public function delete()
    {
        if (!$this->loaded) throw new \PuzzleError("Data belum disimpan di database");

        $d = \Database::readAll("app_evote_kertassuara_paslon", "where paslon=? limit 1", $this->id)->data;
        if (count($d) > 0) throw new \PuzzleError("Paslon sedang digunakan oleh Kertas Suara");

        \Database::deleteRow("app_evote_paslon", "id", $this->id);
        $this->loaded = false;
    }

    public static function getAll()
    {
        return \Database::readAll("app_evote_paslon")->data;
    }
}
?>