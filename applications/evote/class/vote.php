<?php
namespace EVote;

class Vote
{
    public static function prepare(Pemilih $pemilih)
    {
        //Cek expiration
        $expired = \Database::readAll("app_evote_vote_token", "where nim='?' order by created desc", $pemilih->nim)->data[0]["created"] + 600;
        if (time() < $expired) return false;

        //Cek sudah memilih atau belum?
        if (empty(self::siapkan_kertassuara($pemilih))) return false;

        //Generate Token
        $token = rand_str(6, "abcdefghijklmnopqrstuvwxyz");
        if (\Database::newRow(
            "app_evote_vote_token",
            $token,
            $pemilih->nim,
            time()
        )) return $token;
        else return false;
    }

    public static function pilih($token, $kertassuara, $pilihan)
    {
        //Cek apakah token valid?
        $data_token = \Database::readAll("app_evote_vote_token", "where token='?'", $token)->data[0];
        if (!isset($data_token)) {
            throw new \PuzzleError("Token Anda invalid");
        } else {
            $pemilih = new Pemilih($data_token["nim"]);
            $angkatan = $pemilih->angkatan;
            if (in_array($kertassuara, self::cari_kertassuara($angkatan))) {
                $i = new \DatabaseRowInput();
                $i->setField("nim", $pemilih->nim);
                $i->setField("kertassuara", $kertassuara);
                $i->setField("choice", $pilihan == "" ? -1 : $pilihan);
                $i->setField("token", $token);
                try {
                    return \Database::newRowAdvanced("app_evote_vote_choice", $i) ? true : false;
                } catch (\DatabaseError $err) {
                    return false;
                }
            } else {
                throw new \PuzzleError("Anda tidak diperkenankan memilih kertas suara ini.");
            }
        }
    }

    public static function check_token_expiration($token)
    {
        $created = \Database::read("app_evote_vote_token", "created", "token", $token);
        return ($created != "" && ($created + 600) > time());
    }

    public static function siapkan_kertassuara(Pemilih $pemilih)
    {
        $kertassuara = self::cari_kertassuara($pemilih->angkatan);
        $kertassuara_u_pemilih = [];
        foreach ($kertassuara as $k) {
            if (count(\Database::readAll("app_evote_vote_choice", "where nim='?' and kertassuara='?'", $pemilih->nim, $k)->data) < 1) {
                $kertassuara_u_pemilih[] = $k;
            }
        }
        return $kertassuara_u_pemilih;
    }

    public static function cari_kertassuara($angkatan)
    {
        $target_kertassuara = [];
        foreach (\Database::readAll("app_evote_kertassuara_target", "where target='?'", $angkatan)->data as $p) {
            $target_kertassuara[] = $p["kertassuara"];
        }
        return $target_kertassuara;
    }

    public static function ambil_hasil(KertasSuara $kertassuara)
    {
        return \Database::readAllCustom("app_evote_vote_choice", function ($row) {
            return $row["choice"];
        }, "where kertassuara='?'", $kertassuara->id)->data;
    }

    public static function ambil_hasil_grouped(KertasSuara $kertassuara)
    {
        $max_pemilih = \Database::toArray(\Database::exec("
        select b.kertassuara, count(b.kertassuara) as max_pemilih from app_evote_pemilih a
        left join app_evote_kertassuara_target b on a.angkatan = b.target
        where b.kertassuara = '?'
        group by b.kertassuara
        ",$kertassuara->id))->data[0]["max_pemilih"];
        $votes = \Database::toArray(\Database::exec("
        select choice as paslon, count(choice) as yang_memilih from app_evote_vote_choice
        join app_evote_vote_token using(token)
        where kertassuara = '?'
        group by choice
        ",$kertassuara->id))->data;
        $percent = [];
        foreach($votes as $vote){
            if($vote["yang_memilih"] == "") $vote["yang_memilih"] = 0;
            $percent[$vote["paslon"]] = ($vote["yang_memilih"] / $max_pemilih) * 100;
        }
        return $percent;
    }
}
?>