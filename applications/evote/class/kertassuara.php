<?php
namespace EVote;

class KertasSuara implements \JsonSerializable
{
    private $id;
    private $target = [];
    private $judul;
    private $kandidat = [];
    private $bisa_golput = true;

    private $loaded = false;

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "target" => $this->__get("target"),
            "judul" => $this->judul,
            "kandidat" => $this->__get("kandidat"),
            "bisa_golput" => $this->__get("bisa_golput")
        ];
    }

    public function __construct($id = null)
    {
        if ($id !== null) {
            $data = \Database::readAll("app_evote_kertassuara", "where id='?'", $id)->data[0];
            if (isset($data["id"])) {
                $this->id = $data["id"];
                $this->judul = $data["judul"];
                $this->bisa_golput = $data["bisa_golput"] ? true : false;
                $this->loaded = true;
                $this->load_target();
                $this->load_paslon();
            } else {
                throw new \PuzzleError("Kertas Suara tidak ditemukan");
            }
        }
    }

    private function write_kertassuara()
    {
        $a = new \DatabaseRowInput;
        $a->setField("judul", $this->judul);
        $a->setField("bisa_golput", $this->bisa_golput ? 1 : 0);

        if (!$this->loaded) {
            return \Database::newRowAdvanced("app_evote_kertassuara", $a);
        } else {
            return \Database::updateRowAdvanced("app_evote_kertassuara", $a, "id", $this->id);
        }
    }

    private function write_target()
    {
        if (!isset($this->id)) throw new \PuzzleError("Terjadi Kesalahan");
        \Database::deleteRowArg("app_evote_kertassuara_target", "where kertassuara='?'", $this->id);

        foreach ($this->target as $angkatan => $x) {
            \Database::newRow("app_evote_kertassuara_target", $this->id, $angkatan);
        }
    }

    private function load_target()
    {
        if (!isset($this->id)) throw new \PuzzleError("Terjadi Kesalahan");
        foreach (\Database::readAll("app_evote_kertassuara_target", "where kertassuara='?'", $this->id)->data as $d) {
            $this->target[$d["target"]] = 1;
        }
    }

    private function load_paslon()
    {
        if (!isset($this->id)) throw new \PuzzleError("Terjadi Kesalahan");
        foreach (\Database::readAll("app_evote_kertassuara_paslon", "where kertassuara='?' order by id asc", $this->id)->data as $d) {
            $this->kandidat[$d["paslon"]] = 1;
        }
    }

    private function write_paslon()
    {
        if (!isset($this->id)) throw new \PuzzleError("Terjadi Kesalahan");
        \Database::deleteRowArg("app_evote_kertassuara_paslon", "where kertassuara='?'", $this->id);

        foreach ($this->kandidat as $kandidat => $x) {
            \Database::newRow("app_evote_kertassuara_paslon", $this->id, $kandidat);
        }
    }

    public function save()
    {
        if (count($this->target) < 1) throw new \PuzzleError("Masukkan data dengan benar!");
        if (count($this->kandidat) < 1) throw new \PuzzleError("Masukkan data dengan benar!");
        if (!isset($this->judul)) throw new \PuzzleError("Masukkan Judul");

        if ($this->write_kertassuara()) {
            if (!$this->loaded) {
                $this->id = \Database::max("app_evote_kertassuara", "id");
                $this->loaded = true;
            }
        } else {
            return false;
        }
        $this->write_target();
        $this->write_paslon();
        return true;
    }

    public function addTarget($target)
    {
        switch ($target) {
            case "2018":
            case "2017":
            case "2016":
            case "2015":
                break;
            default:
                throw new \PuzzleError("Target Angkatan Salah!");
        }
        $this->target[$target] = 1;
    }

    public function resetTarget()
    {
        $this->target = [];
    }

    public function addKandidat(Paslon $paslon)
    {
        if (isset($this->kandidat[$paslon->id])) throw new \PuzzleError("Paslon sudah ada di dalam kertas");
        $this->kandidat[$paslon->id] = 1;
    }

    public function resetKandidat()
    {
        $this->kandidat = [];
    }

    public function __set($var, $val)
    {
        switch ($var) {
            case "judul":
                $this->judul = htmlentities($val);
                break;
            case "bisa_golput":
                $this->bisa_golput = $val ? true : false;
                break;
        }
    }

    public function __get($var)
    {
        $n = [];
        switch ($var) {
            case "id":
                return $this->id;
            case "target":
                foreach ($this->target as $target => $b) {
                    $n[] = $target;
                }
                return $n;
            case "judul":
                return $this->judul;
            case "bisa_golput":
                return $this->bisa_golput;
            case "kandidat":
                foreach ($this->kandidat as $kandidat => $b) {
                    $n[] = $kandidat;
                }
                return $n;
        }
    }

    public static function getAll()
    {
        return \Database::readAllCustom("app_evote_kertassuara", function ($row) {
            return new KertasSuara($row["id"]);
        })->data;
    }

    public static function delete(KertasSuara $kertas)
    {
        $id = $kertas->id;
        \Database::deleteRow("app_evote_kertassuara", "id", $id);
        \Database::deleteRow("app_evote_kertassuara_paslon", "kertassuara", $id);
        \Database::deleteRow("app_evote_kertassuara_target", "kertassuara", $id);
    }
}
?>