<?php
namespace EVote;

class Pemilih implements \JsonSerializable
{
    private $nama;
    private $nim;
    private $angkatan;

    private $loaded = false;

    public function jsonSerialize()
    {
        return [
            "nama" => $this->nama,
            "nim" => $this->nim,
            "angkatan" => $this->angkatan
        ];
    }

    public function __construct($nim = null)
    {
        if ($nim !== null) {
            try {
                $a = \Database::readAll("app_evote_pemilih", "where nim='?'", $nim)->data[0];
                if ($a["nim"] == "") throw new \PuzzleError("Pemilih tidak Ada!");
                $this->nama = $a["nama"];
                $this->nim = $a["nim"];
                $this->angkatan = $a["angkatan"];
                $this->loaded = true;
            } catch (\PuzzleError $e) {
                throw new \PuzzleError("Pemilih tidak Ada!");
            }
        }
    }

    public function save()
    {
        if ($this->nim == null) throw new \PuzzleError("Mohon mengisi NIM!");
        if ($this->nama == null) throw new \PuzzleError("Mohon mengisi Nama!");
        if ($this->angkatan == null) throw new \PuzzleError("Mohon mengisi Angkatan (2015,2016,2017)!");

        $a = new \DatabaseRowInput;
        $a->setField("nama", $this->nama);
        $a->setField("angkatan", $this->angkatan);
        if ($this->loaded) {
            \Database::updateRowAdvanced("app_evote_pemilih", $a, "nim", $this->nim);
        } else {
            $a->setField("nim", $this->nim);
            \Database::newRowAdvanced("app_evote_pemilih", $a);
        }
    }

    public function __set($var, $val)
    {
        switch ($var) {
            case "nama":
                $this->nama = htmlentities($val);
                break;
            case "angkatan":
                switch ($val) {
                    case "2018":
                    case "2017":
                    case "2016":
                    case "2015":
                        break;
                    default:
                        throw new \PuzzleError("Angkatan Salah!");
                }
                $this->angkatan = $val;
                break;
            case "nim":
                if (ctype_digit($val)) $this->nim = $val;
                else throw new \PuzzleError("NIM Salah!");
                break;
        }
    }

    public function __get($var)
    {
        switch ($var) {
            case "nama":
                return $this->nama;
            case "angkatan":
                return $this->angkatan;
            case "nim":
                return $this->nim;
        }
    }

    public static function retrieveAll()
    {
        $a = self::getWhoDoesntUseFranchise();
        $b = [];
        foreach($a as $d) $b[$d->nim] = $d->belum_memilih;
        return \Database::readAllCustom("app_evote_pemilih", function ($row) use($b) {
            return (object) array_merge($row,[
                "franchise"=> (int) $b[$row["nim"]]
            ]);
        })->data;
    }

    public static function getWhoDoesntUseFranchise()
    {
        return \Database::toCustom(\Database::exec("
        select jatah_memilih.nim, count(jatah_memilih.kertassuara) as belum_memilih 
        from
            (select a.nim, a.kertassuara from app_evote_vote_choice a
            join app_evote_vote_token b on a.token = b.token) as sudah_memilih
        right JOIN 
            (select c.nim, d.kertassuara from app_evote_pemilih c
            left join app_evote_kertassuara_target d on c.angkatan = d.target) as jatah_memilih
        on sudah_memilih.nim = jatah_memilih.nim and sudah_memilih.kertassuara = jatah_memilih.kertassuara
        where sudah_memilih.nim is null
        group by jatah_memilih.nim
        "), function ($row) {
            return (object)$row;
        })->data;
    }

    public static function delete($nim)
    {
        return \Database::deleteRow("app_evote_pemilih", "nim", $nim);
    }
}
?>