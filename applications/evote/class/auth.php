<?php
namespace EVote;

class Auth{
    public static function penjaga_tps($new = null){
        if($new === null){
            $a = \UserData::read("penjaga_tps");
            return $a == "" ? \Accounts::getRootGroupId(USER_AUTH_EMPLOYEE) : $a;
        }else{
            if(\Accounts::authAccess(USER_AUTH_SU))
            return \UserData::store("penjaga_tps",$new,"txt",true);
        }
    }
    public static function bilik_pemilih($new = null){
        if($new === null){
            $a = \UserData::read("bilik_pemilih");
            return $a == "" ? \Accounts::getRootGroupId(USER_AUTH_EMPLOYEE) : $a;
        }else{
            if(\Accounts::authAccess(USER_AUTH_SU))
            return \UserData::store("bilik_pemilih",$new,"txt",true);
        }
    }
    public static function panitia_pusat($new = null){
        if($new === null){
            $a = \UserData::read("panitia_pusat");
            return $a == "" ? \Accounts::getRootGroupId(USER_AUTH_EMPLOYEE) : $a;
        }else{
            if(\Accounts::authAccess(USER_AUTH_SU))
            return \UserData::store("panitia_pusat",$new,"txt",true);
        }
    }
}
?>