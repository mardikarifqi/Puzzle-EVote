<?php
use EVote\Pemilih;
use EVote\Vote;

if ($_POST["submit"] == 1) {
    try {
        switch ($_POST["act"]) {
            case 'cari_pemilih':
                $pemilih = new Pemilih($_POST["nim"]);
                die(json_encode([
                    "success" => true, 
                    "pemilih" => $pemilih
                ]));
            case 'generate_token':
                die(json_encode([
                    "success" => true, 
                    "token" => Vote::prepare(new Pemilih($_POST["pemilih"]))
                ]));
        }
    } catch (PuzzleError $e) {
        if ($_POST["json"] == 1) {
            die(json_encode(["success" => false, "reason" => $e->getMessage()]));
        } else {
            Prompt::postError("Terjadi Kesalahan", true);
            redirect();
        }
    }
}

switch (__getURI(1)) {
    default:
        $appProp->bundle["view"] = "tps.home";
        break;
}
?>