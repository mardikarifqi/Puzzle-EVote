<?php
use EVote\Auth;

switch (__getURI(1)) {
    default:
        if ($_POST["submit"] == 1) {
            Auth::penjaga_tps($_POST["penjaga_tps"]);
            Auth::bilik_pemilih($_POST["bilik_pemilih"]);
            Auth::panitia_pusat($_POST["panitia_pusat"]);
            Prompt::postGood("Pengaturan Disimpan", true);
            redirect();
        }
        $appProp->bundle["view"] = "admin.home";
        break;
}
?>