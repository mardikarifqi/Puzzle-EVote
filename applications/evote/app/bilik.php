<?php
use EVote\Vote;
use EVote\Pemilih;

if ($_POST["submit"] == 1) {
    try {
        switch ($_POST["act"]) {
            case "login":
                if(!Vote::check_token_expiration($_POST["token"])){
                    Prompt::postError("Masa token sudah habis. Silahkan membuat token yang baru",true);
                    redirect();
                }

                $kertassuara = Vote::siapkan_kertassuara(new Pemilih(Database::read("app_evote_vote_token","nim","token",$_POST["token"])));
                if(empty($kertassuara)){
                    Prompt::postError("Anda telah menggunakan hak suara",true);
                    redirect();
                }

                $appProp->bundle["view"] = "bilik.pilih";
                $appProp->bundle["token"] = $_POST["token"];
                $appProp->bundle["kertassuara"] = $kertassuara;
                return;
            case "pilih":
                Vote::pilih($_POST["token"],$_POST["kertassuara"],$_POST["pilihan"]);
                exit;
        }
    } catch (PuzzleError $e) {
        if ($_POST["json"] == 1) {
            die(json_encode(["success" => false, "reason" => $e->getMessage()]));
        } else {
            Prompt::postError("Terjadi Kesalahan", true);
            redirect();
        }
    }
}

switch (__getURI(1)) {
    default:
        $appProp->bundle["view"] = "bilik.home";
        break;
}
?>