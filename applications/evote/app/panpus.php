<?php
use EVote\Pemilih;
use EVote\Paslon;
use EVote\Calon;
use EVote\KertasSuara;
use EVote\Vote;

if ($_POST["submit"] == 1) {
    try {
        switch ($_POST["act"]) {
            // UNTUK DATA PEMILIH
            case "upload_csv":
                if (isset($_FILES["csv"])) {
                    set_time_limit(0);
                    $csv = array_map('str_getcsv', file($_FILES["csv"]["tmp_name"]));
                    unset($csv[0]);
                    foreach ($csv as $b) {
                        $a = new Pemilih();
                        $a->nama = $b[1];
                        $a->nim = $b[0];
                        $a->angkatan = $b[2];
                        $a->save();
                    }
                    Prompt::postGood("Import Berhasil", true);
                    redirect("evote/pemilih");
                }
                break;
            case "tambah_pemilih":
                $a = new Pemilih();
                $a->nama = $_POST["nama"];
                $a->nim = $_POST["nim"];
                $a->angkatan = $_POST["tahun_angkatan"];
                $a->save();
                Prompt::postGood("Disimpan", true);
                redirect("evote/pemilih");
            case "edit_nama_pemilih":
                $data = json_decode($_POST["data"]);
                $a = new Pemilih($data->nim);
                $a->nama = $_POST["nama"];
                $a->save();
                die(json_encode(["success" => true]));
            case "edit_angkatan_pemilih":
                $data = json_decode($_POST["data"]);
                $a = new Pemilih($data->nim);
                $a->angkatan = $_POST["angkatan"];
                $a->save();
                die(json_encode(["success" => true]));
            case "hapus_pemilih":
                die(json_encode(["success" => Pemilih::delete($_POST["nim"]) ? true : false]));
                break;
            // UNTUK DATA PASLON
            case "edit_paslon":
            case "tambah_paslon":
                $paslon = new Paslon($_POST["act"] == "edit_paslon" ? $_POST["id"] : null);
                $paslon->kandidat1 = $_POST["kandidat1"];
                $paslon->kandidat2 = $_POST["kandidat2"];
                $foto = ImageUploader::getFileName("foto1");
                if ($foto != "") $paslon->foto1 = $foto;
                $foto = ImageUploader::getFileName("foto2");
                if ($foto != "") $paslon->foto2 = $foto;
                $paslon->save();
                die(json_encode(["success" => true]));
                break;
            case "edit_calon":
            case "tambah_calon":
                $calon = new Calon($_POST["act"] == "edit_calon" ? $_POST["id"] : null);
                $calon->kandidat1 = $_POST["kandidat1"];
                $foto = ImageUploader::getFileName("foto1");
                if ($foto != "") $calon->foto1 = $foto;
                $calon->save();
                die(json_encode(["success" => true]));
                break;
            case "hapus_pcaslon":
                $paslon = new Paslon($_POST["id"]);
                $paslon->delete();
                die(json_encode(["success" => true]));
                break;
            case "load_data":
                die(json_encode([
                    "success" => true,
                    "data" => new Paslon($_POST["id"])
                ]));
            //UNTUK KERTAS SUARA
            case "load_kertassuara":
                die(json_encode([
                    "success" => true,
                    "data" => new KertasSuara($_POST["id"])
                ]));
            case "tambah_kertassuara":
                $kertas = new KertasSuara();
                $kertas->judul = $_POST["nama"];
                $kertas->bisa_golput = $_POST["bisa_golput"];
                foreach (json_decode($_POST["paslon"], true) as $paslon) {
                    $kertas->addKandidat(new Paslon($paslon));
                }
                foreach (json_decode($_POST["target"], true) as $target) {
                    $kertas->addTarget($target);
                }
                die(json_encode([
                    "success" => $kertas->save()
                ]));
            case "ubah_kertassuara":
                $kertas = new KertasSuara($_POST["id"]);
                $kertas->judul = $_POST["nama"];
                $kertas->bisa_golput = $_POST["bisa_golput"];
                $kertas->resetKandidat();
                $kertas->resetTarget();
                foreach (json_decode($_POST["paslon"], true) as $paslon) {
                    $kertas->addKandidat(new Paslon($paslon));
                }
                foreach (json_decode($_POST["target"], true) as $target) {
                    $kertas->addTarget($target);
                }
                die(json_encode([
                    "success" => $kertas->save()
                ]));
            case "hapus_kertassuara":
                KertasSuara::delete(new KertasSuara($_POST["id"]));
                die(json_encode([
                    "success" => true,
                ]));
        }
    } catch (PuzzleError $e) {
        if ($_POST["json"] == 1) {
            die(json_encode(["success" => false, "reason" => $e->getMessage()]));
        } else {
            Prompt::postError("Terjadi Kesalahan", true);
            redirect();
        }
    }
}

switch (__getURI(1)) {
    case "csvpemilih":
        $tempfile = tempnam(sys_get_temp_dir(), "csv");
        file_put_contents($tempfile, "NIM,Nama,Angkatan");
        IO::streamFile($tempfile, true, "template.csv");
    case "data":
        switch (__getURI(2)) {
            case "pemilih":
                die(json_encode(Pemilih::retrieveAll()));
            default:
                redirect();
        }
        break;
    case "paslon":
        $appProp->bundle["view"] = "panpus.paslon";
        break;
    case "kertassuara":
        $appProp->bundle["view"] = "panpus.kertassuara";
        break;
    case "hitung":
        if (isset($_GET["id"])) {
            try {
                $k = new KertasSuara($_GET["id"]);
                $appProp->bundle = [
                    "view" => "panpus.count.view",
                    "kertassuara" => $k,
                    "hasil" => Vote::ambil_hasil($k),
                    "pub" => IO::publish(my_dir("assets"))
                ];
                UserData::store("sudah_hitung_".$k->id,1,"json",true);
            } catch (PuzzleError $e) {
                Prompt::postError($e->getMessage());
                $appProp->bundle["view"] = "panpus.count.start";
            }
        } else {
            $appProp->bundle["view"] = "panpus.count.start";
        }
        break;
    case "pemilih":
    default:
        $appProp->bundle["view"] = "panpus.pemilih";
        break;
}
?>