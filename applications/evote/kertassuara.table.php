<?php
$a = new DatabaseTableBuilder();

$a->addColumn("id","INT")->setAsPrimaryKey()->defaultValue("AUTO_INCREMENT");
$a->addColumn("judul");
$a->addColumn("bisa_golput","INT(1)")->defaultValue(1);

return $a;
?>