<?php
$a = new DatabaseTableBuilder();

$a->addColumn("id","INT")->setAsPrimaryKey()->defaultValue("AUTO_INCREMENT");
$a->addColumn("kandidat1");
$a->addColumn("kandidat2")->allowNull(true);
$a->addColumn("foto1");
$a->addColumn("foto2")->allowNull(true);
$a->addColumn("tipe","CHAR(1)");

return $a;
?>