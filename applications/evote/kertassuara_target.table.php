<?php
$a = new DatabaseTableBuilder();

$a->addColumn("kertassuara","INT");
$a->addColumn("target","CHAR(4)");

$a->createIndex("id",["kertassuara","target"], "UNIQUE");

return $a;
?>