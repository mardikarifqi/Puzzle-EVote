<?php ob_start()?>
<style>
/* Black */
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 900;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Black.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 900;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-BlackItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 800;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Heavy.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 800;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-HeavyItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 700;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Bold.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 700;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-BoldItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 600;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Medium.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 600;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-MediumItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 500;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Medium.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 500;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-MediumItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 400;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Roman.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 400;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Italic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 300;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Thin.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 300;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-ThinItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 200;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-Light.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 200;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-LightItalic.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 100;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-UltraLight.otf");
}
@font-face {
  font-family: "Neue Helvetica";
  font-weight: 100;
  font-style: italic;
  src: url("<?php echo $tmpl->url?>/fonts/HelveticaNeue-UltraLightItal.otf");
}
</style>
<?php echo Minifier::getCSSFile()?>